package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.QuickContactBadge;

import ukdw.com.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {
    String isilogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref3 = (Button)findViewById(R.id.btnPref3);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        // membaca pref isi login true/false
        isilogin = pref.getString("isilogin","0");
        if(isilogin.equals("1")){
            btnPref3.setText("Logout");
        }else{
            btnPref3.setText("Login");
        }

        //pengisian pref
        btnPref3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isilogin = pref.getString("isilogin","0");
                if(isilogin.equals("0")){
                    editor.putString("isilogin", "1");
                    btnPref3.setText("Logout");
                }else{
                    editor.putString("isilogin", "0");
                    btnPref3.setText("Login");
                }
                editor.commit();
            }
        });
    }
}