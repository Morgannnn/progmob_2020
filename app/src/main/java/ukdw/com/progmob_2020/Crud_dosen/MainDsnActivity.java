package ukdw.com.progmob_2020.Crud_dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.Crud.MahasiswaGetAllActivity;
import ukdw.com.progmob_2020.Crud.MainMhsActivity;

import ukdw.com.progmob_2020.R;

public class MainDsnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dsn);

        Button buttonGetDsn = (Button)findViewById(R.id.buttonGetDsn);
        Button buttonAddDsn = (Button)findViewById(R.id.buttonAddDsn) ;
        Button buttonHapusDsn = (Button)findViewById(R.id.buttonHapusDsn);
        Button buttonUpdateDsn = (Button)findViewById(R.id.buttonUpdateDsn);


        buttonGetDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, DosenGetAllActivity.class);

                startActivity(intent);
            }
        });
        buttonAddDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, DosenAddActivity.class);

                startActivity(intent);
            }
        });
        buttonHapusDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, HapusDsnActivity.class);

                startActivity(intent);
            }
        });
        buttonUpdateDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, DosenUpdateActivity.class);

                startActivity(intent);

            }
        });



    }
}